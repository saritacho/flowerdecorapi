from django.db import models
from datetime import datetime
# from core.users.models import *
from core.products.models import *

# Create your models here.
class order(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    processing_id = models.CharField(max_length=100)
    prefix = models.CharField(max_length=100)
    delivered_by = models.IntegerField()
    shipping_agent_id= models.IntegerField()
    shipping_mode = models.CharField(max_length=150)
    required_date = models.DateTimeField(auto_now_add=True)
    approved_date = models.DateTimeField(auto_now_add=True)
    delivere_date = models.DateTimeField(auto_now_add=True)
    shipped_date = models.DateTimeField(auto_now_add=True)
    completed_date = models.DateTimeField(auto_now_add=True)
    received_date = models.DateTimeField(auto_now_add=True)
    cancelled_date = models.DateTimeField(auto_now_add=True)
    remark = models.CharField(max_length=100)
    token = models.CharField(max_length=100)
    status = models.IntegerField()
    active = models.BooleanField(default=True)
    # owner_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='project_owner_id', null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField(null=True)
    updated_by = models.IntegerField(null=True)