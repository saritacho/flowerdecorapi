from django.db import models
from django.contrib.auth.models import User
# from core.users.models import *
from datetime import datetime

# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=100)
    # company_id = models.ForeignKey(Company, on_delete=models.CASCADE)
    description = models.CharField(max_length=150)
    active = models.BooleanField(default=True)
    # owner_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='project_owner_id', null=True)
    self_activity = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(auto_now_add=True)
    icon = models.CharField(max_length=150,null=True)
    unit_type = models.FloatField(null=True)
    display_order = models.IntegerField(null=True)
    image = models.CharField(max_length=150,null=True)
    created_by = models.IntegerField(null=True)
    updated_by = models.IntegerField(null=True)

   





