from django.apps import AppConfig


class BundletypesConfig(AppConfig):
    name = 'bundletypes'
