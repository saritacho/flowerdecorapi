from django.db import models
from datetime import datetime
# from core.users.models import *
from core.products.models import *

# Create your models here.
class Stock(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    quantity = models.IntegerField()
    measurement = models.CharField(max_length=150)
    active = models.BooleanField(default=True)
    # owner_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='project_owner_id', null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField(null=True)
    updated_by = models.IntegerField(null=True)