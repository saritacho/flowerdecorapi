from django.db import models
from datetime import datetime
from core.order.models import *
from core.products.models import *

# Create your models here.
class orderlog(models.Model):
    order_id = models.ForeignKey(order, on_delete=models.CASCADE)
    remark = models.CharField(max_length=100)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField(null=True)
    updated_by = models.IntegerField(null=True)