from django.apps import AppConfig


class OrderlogConfig(AppConfig):
    name = 'orderlog'
