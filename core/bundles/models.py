from django.db import models
from django.contrib.auth.models import User
# from core.users.models import *
from datetime import datetime

# Create your models here.
class Bundle(models.Model):
    unit_type = models.CharField(max_length=100)
    quantity = models.IntegerField(null=True)
    measurement = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.IntegerField(null=True)
    updated_by = models.IntegerField(null=True)
    active = models.BooleanField(default=True)


   





