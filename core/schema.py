import graphene
from graphql_auth.schema import UserQuery, MeQuery
from graphql_auth import mutations
# import core.users.schema
import core.products.schema
import core.bundles.schema
import core.bundletypes.schema
import core.orderitemstatus.schema
import core.states.schema
import core.city.schema
import core.stock.schema
import core.order_status.schema
import core.order.schema
import core.orderlog.schema







class AuthMutation(graphene.ObjectType):
    register = mutations.Register.Field()
    verify_account = mutations.VerifyAccount.Field()
    resend_activation_email = mutations.ResendActivationEmail.Field()
    send_password_reset_email = mutations.SendPasswordResetEmail.Field()
    password_reset = mutations.PasswordReset.Field()
    password_change = mutations.PasswordChange.Field()
    
    token_auth = mutations.ObtainJSONWebToken.Field()
    verify_token = mutations.VerifyToken.Field()
    refresh_token = mutations.RefreshToken.Field()
    revoke_token = mutations.RevokeToken.Field()


class Query(core.states.schema.Query,core.orderlog.schema.Query,core.city.schema.Query,core.stock.schema.Query,core.order.schema.Query,core.order_status.schema.Query,core.products.schema.Query,core.orderitemstatus.schema.Query,core.bundles.schema.Query,core.bundletypes.schema.Query, UserQuery, MeQuery, graphene.ObjectType):
    pass


class Mutation(AuthMutation, core.bundles.schema.Mutation,core.orderlog.schema.Mutation,core.orderitemstatus.schema.Mutation,core.products.schema.Mutation,core.bundletypes.schema.Mutation,core.states.schema.Mutation,core.city.schema.Mutation,core.stock.schema.Mutation,core.order.schema.Mutation,core.order_status.schema.Mutation,graphene.ObjectType):
    pass

# class Subscription(YourSubscription):
#     pass

schema = graphene.Schema(query=Query, mutation=Mutation)
