import graphene
from django_graphene_permissions import permissions_checker
from django_graphene_permissions.permissions import IsAuthenticated
from graphene import relay, Field, ID, Decimal
from graphene_django.filter import DjangoFilterConnectionField
from django_filters import FilterSet, OrderingFilter
from graphene_django.types import DjangoObjectType
from datetime import datetime
from django.contrib.auth.models import User
from .models import *
from graphql_relay.node.node import from_global_id
from django.utils.dateparse import parse_date, parse_datetime
from django.db.models import F


class ExtendedConnection(graphene.Connection):
    class Meta:
        abstract = True

    total_count = graphene.Int()
    edge_count = graphene.Int()

    def resolve_total_count(root, info, **kwargs):
        return root.length

    def resolve_edge_count(root, info, **kwargs):
        return len(root.edges)


class OrderstatusType(DjangoObjectType):
    pk = graphene.ID(source='id', required=True)

    class Meta:
        model = OrderStatus
        ordering = ['id']
        filter_fields = {
            'name': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.Node,)
        connection_class = ExtendedConnection


class Query(object):
    # Query for Orderstatus all
    Orderstatus = DjangoFilterConnectionField(OrderstatusType, orderBy=graphene.List(of_type=graphene.String))

    @permissions_checker({IsAuthenticated})
    def resolve_Orderstatus(self, info, **kwargs):
        orderBy = kwargs.get('orderBy', None)
        if orderBy:
            return OrderStatus.objects.order_by(*orderBy)
        else:
            return OrderStatus.objects.all()

    # Query for ProjectUser with id
    OrderstatusbyId = Field(OrderstatusType, id=ID(required=True))

    @permissions_checker({IsAuthenticated})
    def resolve_OrderstatusbyId(self, info, **kwargs):
        id = kwargs.get('id')
        return OrderStatus.objects.get(pk=id).order_by('sequence')

  


class OrderstatusInput(graphene.InputObjectType):
    user_id = graphene.Int(description="note user")
    title = graphene.String(max_length=20, description="note title")
    note = graphene.String(max_length=200)
    color = graphene.String(max_length=50, description="color name")
    # sequence = graphene.Int(description="note order")
    type = graphene.String(max_length=50)


class CreateOrderstatus(graphene.Mutation):
    Note = graphene.Field(OrderstatusType)
    Ok = graphene.Boolean(False)

    class Arguments:
        Input = OrderstatusInput(required=True)

    @permissions_checker({IsAuthenticated})
    def mutate(self, info, Input=None):
        if Input.user_id != 0 and Input.user_id is not None:
            User_data = User.objects.get(pk=Input.user_id)
        else:
            User_data = None

        lst_seq = 0
        last_sequence = OrderStatus.objects.filter(user_id=Input.user_id).last()
        if last_sequence:
            lst_seq = int(last_sequence.sequence) + 1
        else:
            lst_seq = 1

        noteData = OrderStatus(user_id=User_data,
                         title=Input.title,
                         note=Input.note,
                         color=Input.color,
                         sequence=lst_seq,
                         type=Input.type
                         )
        noteData.save()
        return CreateOrderstatus(Ok=True, Note=noteData)




class Mutation(graphene.ObjectType):
    CreateOrderstatus = CreateOrderstatus.Field()
   