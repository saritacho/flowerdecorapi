from django.db import models
from datetime import datetime

# Create your models here.
class OrderStatus(models.Model):
    name = models.CharField(max_length=100)
    display_order = models.IntegerField()
    color_code = models.CharField(max_length=150)
    active = models.BooleanField(default=True)
    # owner_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='project_owner_id', null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField(null=True)
    updated_by = models.IntegerField(null=True)