from django.db import models
from django.contrib.auth.models import User
# from core.users.models import *
from datetime import datetime

# Create your models here.
class Orderitemstatus(models.Model):
    name = models.CharField(max_length=100)
    display_order = models.CharField(max_length=150)
    icon = models.CharField(max_length=150,null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.IntegerField(null=True)
    updated_by = models.IntegerField(null=True)
    active = models.BooleanField(default=True)

   





