# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib import admin
from django.urls import path, include  # add this
from django.views.decorators.csrf import csrf_exempt

from graphene_django.views import GraphQLView
# from .city.views import show,city
from  .city import views


urlpatterns = [
    path('admin/', admin.site.urls),    
    path("graphql/", csrf_exempt(GraphQLView.as_view(graphiql=True))),
      # Django admin route
    path("", include("authentication.urls")), # Auth routes - login / register
    path("", include("app.urls")) ,            # UI Kits Html files
    # path("", include("core.urls")),
    # path('show/',show),
    path('city/',views.city)
    
]
